function createPyramid(height){
    let pyramid = [];
    let width = 1 + ((height-1)*2);

    for(let i = 0 ; i < height ; i++){
        let row = [];
        for(let j = 0 ; j < width ; j++){
            
            if((j - i) < 0 || (j + i) >= width){
                row.push("&nbsp;&nbsp;");
            } else {
                row.push("*");
            }
        }
        pyramid.push(row);
    }
    return pyramid;
}

function displayPyramid(pyramid){
    pyramid.forEach(row=>{
        let para = document.createElement("p");
        document.body.appendChild(para);
        row.forEach(letter=>{
            para.innerHTML+=letter;
        })
    })
}

displayPyramid(createPyramid(10));